#!/usr/bin/env python
import json

def json_to_csv(jsonfile, csvfile=None):
    with open(jsonfile) as js:
        j = json.load(js)
    print(HEAD, file=csvfile)
    write_json_section(j['overview'],"Country - UK", csvfile)
    write_json_section(j['countries'],"Country", csvfile)
    return

HEAD='"Area name","Area code","Area type","Reporting date","Daily hospital deaths","Cumulative hospital deaths"'
FORMAT='"%s","%s","%s","%s",%s,%s'

def write_json_section(areadata, Area_Type, csvfile):
    outputrecords = {}
    codes = areadata.keys()
    for code in codes:
      onearea = areadata[code]
      try:
        name = onearea['name']['value']
        rec={}
        for datevalue in onearea['dailyDeaths']:
            dat = datevalue['date']
            rec[dat]={}
            rec[dat]['date']=dat
            rec[dat]['dailyDeaths'] = datevalue['value']
            rec[dat]['name']=name
            rec[dat]['code']=code
        for datevalue in onearea['dailyTotalDeaths']:
            dat = datevalue['date']
            if dat not in rec.keys():
                rec[dat]={}
            rec[dat]['dailyTotalDeaths'] = datevalue['value']
            rec[dat]['date']=dat
            rec[dat]['name']=name
            rec[dat]['code']=code
        outputrecords[code] = rec
      except:
          pass

    for area,record in outputrecords.items():
        for datekey,daterecord in record.items():
            name = daterecord['name']
            code = daterecord['code']
            date = daterecord['date']
            dcc = daterecord.get('dailyDeaths',"")
            dtcc = daterecord.get('dailyTotalDeaths',"")
            print(FORMAT % ( name,code,Area_Type,
                                 date,
                                 dcc,
                                 dtcc),
                  file=csvfile
            )

    return outputrecords
