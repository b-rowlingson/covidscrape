#!/bin/bash

mkdir -p public

echo "<h1>Coronavirus data</h1>" > public/index.html
cat >>public/index.html <<EOF
<p>
This used to pull all the coronovirus JSON files from the GOV site and convert them to CSV, but there's no
more JSONs listed now. Check out <a href="https://publicdashacc.blob.core.windows.net/publicdata?restype=container&comp=list&prefix=data_">the repo listing</a> in case this has changed.
</p>
EOF
exit 0

python getdata.py build 

cd public
echo "<h1>Coronavirus data</h1>" > index.html
cat >>index.html <<EOF
<p>IF you just want the CSV files then there is now a direct link to the cases and deaths CSV from the 
<a href="https://coronavirus.data.gov.uk/">coronavirus dashboard</a>
<ul>
<li><a href="https://coronavirus.data.gov.uk/downloads/csv/coronavirus-cases_latest.csv">Cases</a></li>
<li><a href="https://coronavirus.data.gov.uk/downloads/csv/coronavirus-deaths_latest.csv">Deaths</a></li>
</ul>
</p>

<p>
This is a listing of JSON data files from the gov.uk <a href="https://coronavirus.data.gov.uk/">Coronavirus Dashboard</a>
</p>
<p>
The most recent JSON is also converted to a CSV and put here, but you should <b>really</b> get them from the direct links from the dashboard.
</p>
<p>Code for this is in <a href="https://gitlab.com/b-rowlingson/covidscrape">covidscrape</a> in gitlab. I make no
promises of correctness or timeliness of this listing. If this is not up to date then download the repository code and
run the build script.
</p>
EOF
now=`date`
echo "<p>Last update run: <b>$now</b></p>" >>index.html
echo "<h2>CSV</h2>" >> index.html
echo "<ul>" >>index.html
for f in *.csv ; do
    echo '<li><a href="'$f'">'$f'</a></li>' >> index.html
done
echo "</ul>" >> index.html

echo "<h2>JSON</h2>" >>index.html
echo "<ul>" >>index.html
for f in `ls -1rt *.json` ; do
    echo '<li><a href="'$f'">'$f'</a></li>' >> index.html
done
echo "</ul>" >> index.html
